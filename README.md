 # SecFactory

 # WTF is this 
 This is an open source red team automation platform Built on Go and Apache Kafka

 # How does it work? 
 We have a web frontend that speaks to a backend API which exposes data from various Apache Kafka topics that are populated by the results of the containers launched via our automation platform

 # How do I run this?
run `docker-compose up` 


# TODOs
* TODO: Create a design doc better explaining the workflow
  * How data flows into kafka and what topic names are going to be used as a standard
  * Create a list of nessecary API endpoints
  * Create a list of Tools that should be integrated

* TODO: Get a sick logo for the project

* TODO: Add k3s deployment option

* TODO: Add Ansible Role/playbook for deployment

