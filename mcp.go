package main

import (
	"fmt"
	"os"

	"github.com/confluentinc/confluent-kafka-go/kafka"
)

// TODO: Get YAML or TOML based config file going
// TODO: Integrate https://github.com/hashicorp/go-plugin (Implement gRPC interfaces for sendkafka(), readkafka() and output() functions)
// TODO: Build output functions that will take data/logs/reports and output it to JSON, CSV and XML
// TODO: Write out MVP core modules in the README
// TODO: Get readkafka() in a workable state

func main() {
	//TestCode
	a := "applesauce"
	mytopic := "test"
	var topic *string
	topic = &mytopic
	sendkafka(a, topic)
	readkafka(topic)
}

/* partially working Currently getting This error
panic: runtime error: invalid memory address or nil pointer dereference
[signal SIGSEGV: segmentation violation code=0x1 addr=0x0 pc=0x53b29b]

goroutine 1 [running]:
github.com/confluentinc/confluent-kafka-go/kafka.(*Consumer).Poll(...)
        /home/$USER/go/pkg/mod/github.com/confluentinc/confluent-kafka-go@v1.8.2/kafka/consumer.go:344
main.readkafka(0xc000012300)
*/

// Core function to read from a kafka topic
func readkafka(topic *string) {

	consumer, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": "localhost",
		"group.id":          "",
		"auto.offset.reset": "smallest",
		"topic":             topic})
	ev := consumer.Poll(0)
	switch e := ev.(type) {
	case *kafka.Message:
		fmt.Printf("%% Message on %s:\n%s\n",
			e.TopicPartition, string(e.Value))
	case kafka.PartitionEOF:
		fmt.Printf("%% Reached %v\n", e)
	case kafka.Error:
		fmt.Fprintf(os.Stderr, "%% Error: %v\n", e)
	default:
		fmt.Printf("Ignored %v\n", e)
	}
	if err != nil {
		fmt.Printf("It's borked captain!\nHere's your error:\n %s", err)
	}
}

// Core function to send kafka messages to the topic
func sendkafka(message string, topic *string) {
	hostname, err := os.Hostname()
	p, err := kafka.NewProducer(&kafka.ConfigMap{
		"bootstrap.servers": "localhost",
		"client.id":         hostname,
		"acks":              "all"})

	delivery_chan := make(chan kafka.Event, 10000)

	err = p.Produce(&kafka.Message{
		TopicPartition: kafka.TopicPartition{Topic: topic, Partition: kafka.PartitionAny},
		Value:          []byte(message)},
		delivery_chan,
	)
	if err != nil {
		fmt.Printf("Failed to create producer: %s\n", err)
		os.Exit(1)
	}
	strtopic := *topic
	fmt.Printf("Successfully posted \"%s\" to the topic \"%s\"\n", message, strtopic)
}
